﻿using System;

namespace AirVnB.Models.Utils
{
    public class StringUtil
    {
        public static void CheckLength(string fieldName, string text, int min, int max)
        {
            if (!IsValidLength(fieldName, text, min, max))
            {
                throw new ArgumentException($"Length of '{fieldName}' must be between {min} and {max}.");
            }
        }
        public static bool IsValidLength(string fieldName, string text, int min, int max)
        {
            return min <= text.Length  && text.Length <= max;
        }
    }
}
