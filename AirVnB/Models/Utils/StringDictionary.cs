﻿using System;

namespace AirVnB.Models.Utils
{
    public class StringDictionary
    {
        private string[] dict;
        private Random rand = new Random();

        public StringDictionary(string source, string separator = ",")
            => dict = source.Split(separator);

        public string RandomPick()
            => dict[rand.Next(dict.Length)];
    }
}
