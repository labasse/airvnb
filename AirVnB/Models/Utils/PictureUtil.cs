﻿using System;
using System.IO;
using System.Linq;

namespace AirVnB.Models.Utils
{
    public class PictureUtil
    {
        public static void CheckPath(string image)
        {
            if (!IsValidPath(image))
            {
                throw new ArgumentException("Unsupported picture file format");
            }
        }
        public static bool IsValidPath(string image)
        {
            var extension = Path.GetExtension(image);

            return ".jpg;.jpeg;.png;.gif".Split(";").Contains(extension);
        }
    }
}
