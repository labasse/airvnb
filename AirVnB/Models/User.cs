﻿using AirVnB.Models.Utils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AirVnB.Models
{
    public class User : IEntityWithId
    {
        public const int MinFieldLength = 2, MaxFieldLength = 50;

        public User(int? id, string lastName, string firstName, string picture, string location)
        {
            // Validation method #1 : Using exceptions (dismissed)
            Id = id;
            LastName = lastName;
            FirstName = firstName;
            Picture = picture;
            Location = location;
        }
  
        public int? UniqueId => Id;

        // Validation method #2
        public bool IsValid
        {
            get
            {
                // Vérification de tous les champs
                return PictureUtil.IsValidPath(Picture)
                    && StringUtil.IsValidLength(nameof(LastName), LastName, MinFieldLength, MaxFieldLength)
                    && StringUtil.IsValidLength(nameof(FirstName), FirstName, MinFieldLength, MaxFieldLength);
            }
        }

        public void InitWithUniqueId<T>(IEnumerable<T> allusers) where T : IEntityWithId
        {
            Id = allusers.Max(u => u.UniqueId) + 1;
        }

        public int? Id { get; private set; }

        // Validation method #3 : https://docs.microsoft.com/fr-fr/aspnet/core/mvc/models/validation?view=aspnetcore-3.1#required-attribute
        [StringLength(50, MinimumLength = 2, ErrorMessage = "LastName size must be between 2 and 50")]
        public string LastName { get; private set; }

        [StringLength(50, MinimumLength = 2, ErrorMessage = "FirstName size must be between 2 and 50")]
        public string FirstName { get; private set; }

        public string Location { get; private set; }

        [RegularExpression(@"[-_A-Za-z0-9]+\.(jpg|jpeg|png|gif)")]
        public string Picture { get; private set; }

    }
}
