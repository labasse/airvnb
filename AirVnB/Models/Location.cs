﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AirVnB.Models
{
    public class Location : IEntityWithId
    {
        public Location(int? id, string name, int? zipCode)
        {
            Id = id;
            Name = name;
            ZipCode = zipCode;
        }
        public void InitWithUniqueId<T>(IEnumerable<T> alllocations) where T : IEntityWithId
        {
            Id = alllocations.Max(u => u.UniqueId) + 1;
        }
        public int? Id { get; private set; }

        [Required, StringLength(50, MinimumLength = 2, ErrorMessage ="Name size must be between 2 an 50")]
        public string Name { get; private set; }

        [Range(1000, 98890)]
        public int? ZipCode { get; private set; }

        public int? UniqueId => Id;
    }
}
