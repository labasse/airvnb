﻿using AirVnB.Models.Utils;
using Newtonsoft.Json;

namespace AirVnB.Models
{
    public class Aperitif
    {
        public const int MinTitleLength = 2, MaxTitleLength = 50;

        private static int nextId = 0;

       public Aperitif(string title, string image, int maxGuestNum, decimal price, User organiser)
        {
            StringUtil.CheckLength(nameof(title), title, MinTitleLength, MaxTitleLength);
            PictureUtil.CheckPath(image);
            Id = ++nextId;
            Title = title;
            Image = image;
            GuestNum = 0;
            MaxGuest = maxGuestNum;
            Price = price;
            Organiser = organiser;
        }
        public int Id
        {
            get; private set;
        }
        public string Title
        {
            get; private set;
        }
        public string Image
        {
            get; private set;
        }
        public int GuestNum
        {
            get; private set;
        }
        public int MaxGuest
        {
            get; private set;
        }
        public decimal Price
        {
            get; private set;
        }

        public bool IsFull
        {
            get
            {
                return GuestNum == MaxGuest;
            }
        }
        [JsonIgnore]
        public User Organiser
        {
            get; private set;
        }
        public int OrganiserId => Organiser.Id.Value;
        public string OrganiserName => $"{Organiser.FirstName[0]}.{Organiser.LastName}";
    }
}
