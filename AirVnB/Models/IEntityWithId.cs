﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AirVnB.Models
{
    public interface IEntityWithId
    {
        int? UniqueId { get; }
        void InitWithUniqueId<T>(IEnumerable<T> list) where T : IEntityWithId;
    }
}
