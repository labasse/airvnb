﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirVnB.Controllers
{
    public class PagingHelper
    {
        private const int MaxPageSize = 20;

        private static int Quantity(int start, int end) => end - start + 1;

        public static IEnumerable<T> ProcessGet<T>(IQueryCollection queryParams, IEnumerable<T> entities, int entityCount, IHeaderDictionary responseHeaders)
        {
            var first = 0;
            var last = MaxPageSize - 1;

            if (queryParams.ContainsKey("range"))
            {
                var range = queryParams["range"];

                if (range.Count != 1)
                {
                    throw new ArgumentOutOfRangeException("range", "Bad range request parameter number");
                }
                var numbers = range[0].Split("-");

                if (numbers.Length != 2 ||
                    !int.TryParse(numbers[0], out first) ||
                    !int.TryParse(numbers[1], out last))
                {
                    throw new ArgumentOutOfRangeException("range", "Misformatted request range");
                }
                if (first < 0 || last < first || Quantity(first, last) > MaxPageSize)
                {
                    throw new ArgumentOutOfRangeException("range", "Bad request range bounds");
                }
            }
            if (last >= entityCount)
            {
                last = entityCount - 1;
            }
            if (first >= entityCount)
            {
                first = last + 1;
            }
            responseHeaders["Content-Range"] = $"{first}-{last}/{entityCount}";
            responseHeaders["Accept-Range"] = $"{typeof(T).Name.ToLower()} {MaxPageSize}";
            return entities.Skip(first).Take(Quantity(first, last));
        }
    }
}
