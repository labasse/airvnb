﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirVnB.Models;
using Microsoft.AspNetCore.Mvc;

namespace AirVnB.Controllers
{
    [Route("api/[controller]")]
    public class LocationsController : Controller
    {
        private DataContext _dataContext;

        public LocationsController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(PagingHelper.ProcessGet(
                    Request.Query,
                    _dataContext.AllLocations,
                    _dataContext.LocationsCount,
                    Response.Headers
                ));
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpGet("{id}")]
        public ActionResult<Location> Get(int id)
        {
            var result = _dataContext.AllLocations.FirstOrDefault(loc => loc.Id == id);

            if (result == null)
            {
                return NotFound("Unable to find this location");
            }
            else
            {
                return result;
            }
        }
        // TODO : Put, Delete => DataContext doit contenir un Save(Location )
        [HttpPost]
        public ActionResult<Location> Post([FromBody] Location newLocation)
        {
            if (newLocation == null)
            {
                return BadRequest("No user provided");
            }
            if (newLocation.Id.HasValue)
            {
                return BadRequest("Unexpected user id");
            }
            if (!ModelState.IsValid) //if(!newUser.IsValid)
            {
                return BadRequest(ModelState);
            }
            _dataContext.Save(newLocation);
            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}/{newLocation.Id}");

            return Created(location, newLocation);
        }
        [HttpPut("{id}")]
        public ActionResult<Location> Put(int id, [FromBody] Location location)
        {
            if (location == null)
            {
                return BadRequest("No location provided");
            }
            if (!location.Id.HasValue || id != location.Id.Value)
            {
                return BadRequest("Inconsistent location id");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _dataContext.Save(location);
                return Ok(location);
            }
            catch (ArgumentException)
            {
                return NotFound("Unknown location");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _dataContext.RemoveLocation(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("Unknown location");
            }
        }
    }
}
