﻿using System;
using System.Linq;
using AirVnB.Models;
using Microsoft.AspNetCore.Mvc;

namespace AirVnB.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private DataContext _dataContext;

        public UsersController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok(PagingHelper.ProcessGet(
                    Request.Query,
                    _dataContext.AllUsers,
                    _dataContext.UsersCount,
                    Response.Headers
                ));
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            var result = _dataContext.AllUsers.FirstOrDefault(user => user.Id == id);

            if (result == null)
            {
                return NotFound("Unable to find this user");
            }
            else
            {
                return result;
            }
        }

        [HttpPost]
        public ActionResult<User> Post([FromBody] User newUser)
        {
            if (newUser == null)
            {
                return BadRequest("No user provided");
            }
            if (newUser.Id.HasValue)
            {
                return BadRequest("Unexpected user id");
            }
            if(!ModelState.IsValid) //if(!newUser.IsValid)
            {
                return BadRequest(ModelState);
            }
            _dataContext.Save(newUser);
            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}/{newUser.Id}");

            return Created(location, newUser);
        }
        [HttpPut("{id}")]
        public ActionResult<User> Put(int id, [FromBody] User user) 
        {
            if (user == null)
            {
                return BadRequest("No user provided");
            }
            if (!user.Id.HasValue || id != user.Id.Value)
            {
                return BadRequest("Inconsistent user id");
            }
            if (!ModelState.IsValid) //if(!user.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _dataContext.Save(user);
                return Ok(user);
            }
            catch(ArgumentException)
            {
                return NotFound("Unknown user");
            }
        }
        [HttpDelete("{id}")] 
        public ActionResult Delete(int id)
        {
            try
            {
                _dataContext.RemoveUser(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("Unknown user");
            }
        }
    }
}
