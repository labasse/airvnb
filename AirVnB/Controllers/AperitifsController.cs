﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirVnB.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AirVnB.Controllers
{
    [Route("api/[controller]")]
    public class AperitifsController : Controller
    {
        private DataContext _dataContext;

        public AperitifsController(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        // GET: api/<controller>
        [HttpGet]
        public ActionResult Get()
        {
            var resultat = _dataContext.AllAperitifs;
            var total = _dataContext.AperitifsCount;

            // - Step 1 : Retreive the 'user' parameter (cf getting the 'range' parameter)
            if (Request.Query.ContainsKey("user"))
            {
                var user = Request.Query["user"];

                if (user.Count != 1)
                {
                    return BadRequest("Bad user parameter number");
                }
                // - Step 1bis : Convert user[0] to int (TryParse)
                int userId;

                if (!int.TryParse(user[0], out userId))
                {
                    return BadRequest("Misformatted user id");
                }
                // - Step 2 : Filter with the DataContext.Instance.AllAperos.Where() (cf FirstOrDefault)
                resultat = resultat.Where(aperitif => aperitif.OrganiserId == userId);
                total = resultat.Count();
            }

            try
            {
                return Ok(PagingHelper.ProcessGet(Request.Query, resultat, total, Response.Headers));
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public ActionResult<Aperitif> Get(int id)
        {
            var result = _dataContext.AllAperitifs.FirstOrDefault(aperitif => aperitif.Id == id);

            if (result == null)
            {
                return NotFound("Unable to find this aperitif");
            }
            else
            {
                return result;
            }
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
